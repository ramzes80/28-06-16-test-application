'use strict';
/*
globals
require:false
*/

var gulp = require('gulp');

var less = require('gulp-less'),
	concat = require('gulp-concat'),  
	cssnano = require('gulp-cssnano'),
	rename = require("gulp-rename"),
	uglify = require('gulp-uglify'),
	ngAnnotate = require('gulp-ng-annotate'),
	sourcemaps = require('gulp-sourcemaps'),
	templateCache = require('gulp-angular-templatecache'),
	closure = require('gulp-jsclosure'),
	mainBowerFiles = require('main-bower-files'),
  gulpCopy = require('gulp-copy'),
  jshint = require("gulp-jshint"),
	gulpFilter = require('gulp-filter');

var src = {
  less: ['./app/less/**/*.less'],
  css: ['./src/**/*.css'],
  js: ['./app/js/**/*.js'],
  bower: ['bower.json', '.bowerrc']
};

var jshintConfig = {
  "strict": true,
  "curly": true,
  "undef": true,
  "laxcomma": true,
  "eqeqeq": true,
  "unused": true,
  "eqnull": true,
  "browser": true,
  "devel": true,
  "jasmine": true,
  "jquery": true,
  "node": true,
  "globals": {
    "angular": true,
    "describe": true,
    "it": true,
    "expect": true,
    "beforeEach": false,
    "afterEach": false,
    "inject": false,
    "browser": true,
    "by": true,
    "element": true,
    "L": true,
    "moment": true
  }
};

src.styles = src.less.concat(src.css);

var publishdir = './public/';
var dist = {
  all: [publishdir + './**/*'],
  css: publishdir + '/css/',
  js: publishdir + '/js/',
  fonts: publishdir+ '/fonts/',
  vendor: publishdir + '/vendors/'
};

gulp.task('bower', function() {
  var jsFilter = gulpFilter('**/*.js', { restore: true });
  var cssFilter = gulpFilter('**/*.css', { restore: true });
  var lessFilter = gulpFilter('**/*.less', { restore: true });

  gulp.src(mainBowerFiles())
    .pipe(jsFilter)
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(rename({
        suffix: ".min"
     }))
    .pipe(gulp.dest(dist.js))
    .pipe(jsFilter.restore)
    .pipe(lessFilter)
    .pipe(less())
    .pipe(lessFilter.restore)
    .pipe(cssFilter)
    .pipe(concat('vendor.css'))
    .pipe(cssnano())
    .pipe(rename({
        suffix: ".min"
    }))
    .pipe(gulp.dest(dist.css))
    .pipe(cssFilter.restore)
  ;

  gulp.src('./bower_components/bootstrap/fonts/*')
    .pipe(gulpCopy("./public/fonts", { prefix: 3}))
  ;
});


function buildLess() {
	gulp.src(src.less)
	.pipe(less())
	.pipe(concat('common.css'))
	.pipe(cssnano())
	.pipe(rename({suffix: ".min"}))
	.pipe(gulp.dest('./public/css'));

  gulp.src("./app/lib/**/*.css")
    .pipe(gulpCopy("./public/css",{prefix: 3}))
  ;
}

function buildJS() {
	gulp.src(src.js)
	.pipe(sourcemaps.init())
	.pipe(concat('app.js'))
	.pipe(closure({angular: true}))
	.pipe(ngAnnotate())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('./public/js'));

  gulp.src("./app/lib/**/*.js")
    .pipe(gulpCopy("./public/js",{prefix: 3}))
  ;
}

gulp.task('less', buildLess);
gulp.task('js', buildJS);


gulp.task('html', function () {
   gulp.src('./app/templates/**/*.html')
    .pipe(templateCache('templatescache.js', { module:'templatescache', standalone:true }))
    .pipe(gulp.dest('./public/js'));
});

gulp.task("img", function () {
  gulp.src(['./app/img/**/*'])
    .pipe(gulpCopy('./public/img',{prefix: 2}))
  ;
});

gulp.task("jshint", function () {
  return gulp.src([("app/js/**/*.js")])
    .pipe(jshint(jshintConfig))
    .pipe(jshint.reporter("default"))
    .pipe(jshint.reporter("fail"));
});


gulp.task("default", ['bower', 'less', 'html', 'js', 'img']);

