# Hello world application

## System Requirements

Install NodeJS .

Install bower, gulp, jshint, http-server from NPM repos.

## Dependency Installation

`npm install`

`bower install`

## Build

`gulp`

## Run

`npm start # localhost:8080`

## Задачи

1) В gulpfile имеются задачи js,less,html для сборки скриптов, стилей и шаблонов соответственно. 
Необходимо добавить задачу для их автоматического запуска при изменении исходных файлов в директориях app/js,app/less,app/templates

2) Необходимо вынести функцию greetString в отдельный сервис, с целью использования в разных котроллерах

3) Необходимо заменить стандартный алерт показа введенного слова на modal window используя angular-bootstrap и сервис greetString

4) Необходимо отцентрировать форму на странице по вертикали и горизонтали

5) При клике на текст **Enter a word** фокус должен перемещаться в текстовый инпут.