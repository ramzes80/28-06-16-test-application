angular.module('testApp')
    .controller('HelloCtrl', [
        '$scope',
        function ($scope) {
            'use strict';

            function greetString(string) {
                return "Hello, " + string;
            }

            $scope.word = "World";

            $scope.hello = function () {
              window.alert(greetString($scope.word));
            };
        }
    ])
;